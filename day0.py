from typing import List
import numpy as np
from filterpy.kalman import KalmanFilter


def bbox_iou(bbox1, bbox2):
    # Calculate the coordinates of the intersection rectangle
    x1 = max(bbox1[0], bbox2[0])
    y1 = max(bbox1[1], bbox2[1])
    x2 = min(bbox1[2], bbox2[2])
    y2 = min(bbox1[3], bbox2[3])
    
    # If the intersection is empty, return 0
    if x2 < x1 or y2 < y1:
        return 0.0
    
    # Calculate the area of intersection rectangle
    intersection_area = (x2 - x1 + 1) * (y2 - y1 + 1)
    
    # Calculate the area of both bounding boxes
    bbox1_area = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
    bbox2_area = (bbox2[2] - bbox2[0] + 1) * (bbox2[3] - bbox2[1] + 1)
    
    # Calculate the union area
    union_area = bbox1_area + bbox2_area - intersection_area
    
    # Calculate the IoU
    iou = intersection_area / union_area
    
    return iou


class ObjectTracker:
    def __init__(self, object_id, bbox, class_label):
        """
        Initialize the ObjectTracker class.

        Args:
            object_id (int): Unique ID for the object.
            bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                         coordinates in image coordinates.
            class_label (str): The class label of the object.
        """
        self.object_id = object_id
        self.class_label = class_label
        self.kf = KalmanFilter(dim_x=4, dim_z=2)
        self.kf.x = np.array([bbox[0], bbox[1], 0, 0]).T
        self.kf.P *= 10.
        self.kf.R = np.diag([5., 5.])
        self.kf.F = np.array([[1., 0., 1., 0.],
                              [0., 1., 0., 1.],
                              [0., 0., 1., 0.],
                              [0., 0., 0., 1.]])
        self.kf.H = np.array([[1., 0., 0., 0.],
                              [0., 1., 0., 0.]])
        self.kf.Q = np.diag([0.1, 0.1, 0.01, 0.01])
        self.age = 1
        self.time_since_update = 0
        self.history = []
        self.hits = 1
        self.hit_streak = 1
        self.miss_streak = 0
        self.is_confirmed = False
        self.is_deleted = False


    def matches(self, bbox, class_label):
        # Check if the given bbox and class_label match the current tracker's bbox and class_label
        if self.class_label == class_label and bbox_iou(self.kf.x, bbox) > 0.5:
            return True
        else:
            return False
        

    def matches_second_version(self, bbox):
        """
        Determine if a new bounding box matches this object.

        Args:
            bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                            coordinates in image coordinates.

        Returns:
            bool: True if the bounding box matches this object, False otherwise.
        """
        if self.is_confirmed:
            # Calculate IoU between current object and new bounding box
            iou = bbox_iou(self.bbox, bbox)

            # Check if IoU is above a threshold
            if iou > 0.5:
                return True
        else:
            # Calculate Euclidean distance between current object and new bounding box
            dist = np.linalg.norm(self.kf.x[:2] - np.array([bbox[0], bbox[1]]))

            # Check if distance is below a threshold
            if dist < 50:
                return True

        return False


    # def mark_lost(self):
    #     self.lost = True

    def mark_lost(self):
        """
        Mark the object as lost.
        """
        self.is_confirmed = False
        self.time_since_update = 0
        self.miss_streak += 1

        if self.miss_streak > 3:
            self.is_deleted = True

    def is_lost(self):
        """
        Check if the object is lost.

        Returns:
            bool: True if the object is lost, False otherwise.
        """
        return self.time_since_update > 3 * self.hit_streak
    

    def predict(self):
        self.kf.predict()
        self.time_since_update += 1
        if self.hit_streak >= 1:
            self.history = []
        self.hit_streak = 0
        if self.time_since_update > self.age:
            self.is_confirmed = False

    def update(self, bbox):
        self.kf.update(bbox)
        self.history.append(self.kf.x)
        self.time_since_update = 0
        self.hits += 1
        self.hit_streak += 1
        if not self.is_confirmed:
            if self.hits >= self.age:
                self.is_confirmed = True
        if len(self.history) > self.age:
            del self.history[0]

    def get_state(self):
        return self.kf.x[:2].reshape((2,))

    def get_covariance(self):
        return self.kf.P[:2, :2]


def process_frames(frames: List[List[dict]]) -> None:
    # Initialize an instance of ObjectTracker for each object in the first frame
    trackers = [ObjectTracker(obj['object_id'], obj['bbox'], obj['class_label']) for obj in frames[0]]
    
    # Loop over all frames after the first frame
    for i in range(1, len(frames)):
        current_frame = frames[i]
        
        # Update each tracker with the detections in the current frame
        for tracker in trackers:
            detection = None
            
            # Find the detection in the current frame that matches the current tracker
            for obj in current_frame:
                if tracker.matches(obj['bbox'], obj['class_label']):
                    detection = obj
                    break
            
            # Update the tracker with the detection, if found
            if detection:
                tracker.update(detection['bbox'])
            # If no matching detection is found, mark the tracker as lost
            else:
                tracker.mark_lost()
        
        # Add new trackers for any detections in the current frame that do not match any existing trackers
        for obj in current_frame:
            matched = False
            
            for tracker in trackers:
                if tracker.matches(obj['bbox'], obj['class_label']):
                    matched = True
                    break
            
            if not matched:
                trackers.append(ObjectTracker(obj['object_id'], obj['bbox'], obj['class_label']))
        
        # Remove any lost trackers
        trackers = [tracker for tracker in trackers if not tracker.is_lost()]
    
    return trackers


if __name__ == "__main__":
    dummy_frames = [
        # Frame 1
        [
            {'object_id': 1, 'bbox': [100, 200, 150, 250], 'class_label': 'car'},
            {'object_id': 2, 'bbox': [300, 100, 400, 200], 'class_label': 'truck'},
            {'object_id': 3, 'bbox': [500, 300, 550, 350], 'class_label': 'car'},
        ],
        # Frame 2
        [
            {'object_id': 1, 'bbox': [110, 190, 160, 240], 'class_label': 'car'},
            {'object_id': 2, 'bbox': [310, 110, 410, 210], 'class_label': 'truck'},
            {'object_id': 4, 'bbox': [400, 150, 450, 200], 'class_label': 'truck'},
            {'object_id': 5, 'bbox': [600, 400, 650, 450], 'class_label': 'car'},
        ],
        # Frame 3
        [
            {'object_id': 1, 'bbox': [120, 180, 170, 230], 'class_label': 'car'},
            {'object_id': 4, 'bbox': [420, 140, 470, 190], 'class_label': 'truck'},
            {'object_id': 5, 'bbox': [610, 390, 660, 440], 'class_label': 'car'},
            {'object_id': 6, 'bbox': [700, 300, 750, 350], 'class_label': 'car'},
        ],
        # Frame 4
        [
            {'object_id': 1, 'bbox': [130, 170, 180, 220], 'class_label': 'car'},
            {'object_id': 4, 'bbox': [430, 130, 480, 180], 'class_label': 'truck'},
            {'object_id': 6, 'bbox': [710, 290, 760, 340], 'class_label': 'car'},
            {'object_id': 7, 'bbox': [900, 200, 950, 250], 'class_label': 'truck'},
        ],
        # Frame 5
        [
            {'object_id': 1, 'bbox': [140, 160, 190, 210], 'class_label': 'car'},
            {'object_id': 4, 'bbox': [440, 120, 490, 170], 'class_label': 'truck'},
            {'object_id': 6, 'bbox': [720, 280, 770, 330], 'class_label': 'car'},
            {'object_id': 7, 'bbox': [910, 190, 960, 240], 'class_label': 'truck'},
            {'object_id': 8, 'bbox': [1100, 300, 1150, 350], 'class_label': 'car'},
        ]
    ]

    trackers = process_frames(frames=dummy_frames)
    print("done")
