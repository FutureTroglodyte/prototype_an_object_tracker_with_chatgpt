# Day 0 - 2023-03-31  21:42 PM to 00:37 AM

## TL;DR

I initially asked a very complex question,

> Given a Dataset containing video sequences, a corresponding Pytorch Dataloader, and a working Object Detection Model, that returns 2D BoundingBoxes for n classes in Image coordinates. What do I need to create an Object Tracker on top, so that it is possible to:
>     1. get velocity of detected objects (also MotionModel)
>     2. smooth out OD predictions over time - No jittering
>     3. correct false OD predictions via consecutive frames

and got a rather complex code, that runs without errors after a few minor adjustments and chat iterations. But I am convinced yet, that it works. 

## Planning

Startet Planning the Experiment:

![day0_ideas](day0_ideas.png)

## Conversation

#### Me:
Given a Dataset containing video sequences, a corresponding Pytorch Dataloader, and a working Object Detection Model, that returns 2D BoundingBoxes for n classes in Image coordinates. What do I need to create an Object Tracker on top, so that it is possible to:
1. get velocity of detected objects (also MotionModel)
2. smooth out OD predictions over time - No jittering
3. correct false OD predictions via consecutive frames

#### ChatGPT:
To create an Object Tracker that meets the requirements you listed, you will need to follow these steps:

1.  Define a class to represent the objects you want to track. This class should contain the following information:
	-   A unique ID for the object
	-   The bounding box coordinates in image coordinates
	-   The class label of the object
	-   A Kalman Filter to estimate the position and velocity of the object
1.  Modify your existing Object Detection model to return the class label, bounding box coordinates, and confidence score for each detected object.
2.  Create an instance of the Object Tracker class for each detected object in each frame. 
4.  Use the Kalman Filter to estimate the velocity of each object by computing the change in position over time.
5.  Use a simple moving average filter to smooth out the Object Detection predictions over time. This will help reduce jittering.
6.  Implement an algorithm to correct false Object Detection predictions based on consecutive frames. For example, if an object is detected in one frame but not in the next, you can assume that the initial detection was a false positive and remove the object from the list of tracked objects.
7.  Update the position and velocity estimates for each tracked object in each frame using the Kalman Filter.
8.  Finally, you can output the tracked objects along with their velocities and class labels for each frame.

You can use PyTorch to implement the Object Tracker, and you will need to make use of the PyTorch DataLoader to load the video sequences. The Object Tracker can be trained using supervised learning techniques, such as with labeled datasets that contain ground truth object trajectories, or unsupervised learning techniques, such as with the Kalman Filter.

#### Me:
Can you define just this class in 1. for me in python code?

#### ChatGPT:
Certainly! Here is an example implementation of the ObjectTracker class in Python
```python
import numpy as np
from filterpy.kalman import KalmanFilter

class ObjectTracker:
    def __init__(self, object_id, bbox, class_label):
        """
        Initialize the ObjectTracker class.

        Args:
            object_id (int): Unique ID for the object.
            bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                         coordinates in image coordinates.
            class_label (str): The class label of the object.
        """
        self.object_id = object_id
        self.class_label = class_label
        self.kf = KalmanFilter(dim_x=4, dim_z=2)
        self.kf.x = np.array([bbox[0], bbox[1], 0, 0]).T
        self.kf.P *= 10.
        self.kf.R = np.diag([5., 5.])
        self.kf.F = np.array([[1., 0., 1., 0.],
                              [0., 1., 0., 1.],
                              [0., 0., 1., 0.],
                              [0., 0., 0., 1.]])
        self.kf.H = np.array([[1., 0., 0., 0.],
                              [0., 1., 0., 0.]])
        self.kf.Q = np.diag([0.1, 0.1, 0.01, 0.01])
        self.age = 1
        self.time_since_update = 0
        self.history = []
        self.hits = 1
        self.hit_streak = 1
        self.miss_streak = 0
        self.is_confirmed = False
        self.is_deleted = False

    def predict(self):
        self.kf.predict()
        self.time_since_update += 1
        if self.hit_streak >= 1:
            self.history = []
        self.hit_streak = 0
        if self.time_since_update > self.age:
            self.is_confirmed = False

    def update(self, bbox):
        self.kf.update(bbox)
        self.history.append(self.kf.x)
        self.time_since_update = 0
        self.hits += 1
        self.hit_streak += 1
        if not self.is_confirmed:
            if self.hits >= self.age:
                self.is_confirmed = True
        if len(self.history) > self.age:
            del self.history[0]

    def get_state(self):
        return self.kf.x[:2].reshape((2,))

    def get_covariance(self):
        return self.kf.P[:2, :2]
```

This class implements a Kalman Filter to estimate the position and velocity of an object over time. The object is represented by a unique ID, a bounding box in image coordinates, and a class label. The Kalman Filter is initialized with the object's initial position and velocity, and is used to predict the object's position and velocity in each frame. The class also keeps track of various statistics about the object, such as the number of hits and misses, and whether the object has been confirmed or deleted.

#### Me:
Can you provide me reasonable dummy input data to test that classes functionality. Let's say a sequence of 5 frames, each containing of detected objects with object_id, bbox, class_label?

#### ChatGPT:
Can you provide me reasonable dummy input data to test that classes functionality. Let's say a sequence of 5 frames, each containing of detected objects with object_id, bbox, class_label?

```python
[
    # Frame 1
    [
        {'object_id': 1, 'bbox': [100, 200, 150, 250], 'class_label': 'car'},
        {'object_id': 2, 'bbox': [300, 100, 400, 200], 'class_label': 'truck'},
        {'object_id': 3, 'bbox': [500, 300, 550, 350], 'class_label': 'car'},
    ],
    # Frame 2
    [
        {'object_id': 1, 'bbox': [110, 190, 160, 240], 'class_label': 'car'},
        {'object_id': 2, 'bbox': [310, 110, 410, 210], 'class_label': 'truck'},
        {'object_id': 4, 'bbox': [400, 150, 450, 200], 'class_label': 'truck'},
        {'object_id': 5, 'bbox': [600, 400, 650, 450], 'class_label': 'car'},
    ],
    # Frame 3
    [
        {'object_id': 1, 'bbox': [120, 180, 170, 230], 'class_label': 'car'},
        {'object_id': 4, 'bbox': [420, 140, 470, 190], 'class_label': 'truck'},
        {'object_id': 5, 'bbox': [610, 390, 660, 440], 'class_label': 'car'},
        {'object_id': 6, 'bbox': [700, 300, 750, 350], 'class_label': 'car'},
    ],
    # Frame 4
    [
        {'object_id': 1, 'bbox': [130, 170, 180, 220], 'class_label': 'car'},
        {'object_id': 4, 'bbox': [430, 130, 480, 180], 'class_label': 'truck'},
        {'object_id': 6, 'bbox': [710, 290, 760, 340], 'class_label': 'car'},
        {'object_id': 7, 'bbox': [900, 200, 950, 250], 'class_label': 'truck'},
    ],
    # Frame 5
    [
        {'object_id': 1, 'bbox': [140, 160, 190, 210], 'class_label': 'car'},
        {'object_id': 4, 'bbox': [440, 120, 490, 170], 'class_label': 'truck'},
        {'object_id': 6, 'bbox': [720, 280, 770, 330], 'class_label': 'car'},
        {'object_id': 7, 'bbox': [910, 190, 960, 240], 'class_label': 'truck'},
        {'object_id': 8, 'bbox': [1100, 300, 1150, 350], 'class_label': 'car'},
    ]
]
```

#### Me:
In practice, how do we get this object_id from the Object Detection Model. Isn't that something the Object Tracker should provide?

#### ChatGPT:
The object ID is typically not provided directly by the object detection model. Instead, the object ID is usually generated by the object tracker, based on the detected objects in consecutive frames.

In a typical object tracking pipeline, the object detection model is used to detect objects in the first frame of a video sequence. For each detected object, the object tracker generates a unique object ID and initializes a new track for that object. In subsequent frames, the object detection model is used again to detect objects in the current frame, and the object tracker matches these detections with existing tracks based on their location and other features. If a detection does not match any existing tracks, the object tracker will assign a new object ID and create a new track for that detection.

So to answer your question, the object ID is something that the object tracker should provide, based on its tracking algorithm and the detected objects in consecutive frames. The object detection model's role is to detect objects and provide their bounding boxes and class labels to the object tracker, which then assigns unique object IDs and tracks the objects over time.

#### Me:
Can you give me a function that processes each cosecutive frame of your example input data with your ObjectTracker class?

#### ChatGPT:
Sure! Here's an example function that processes each consecutive frame of the example input data using the ObjectTracker class:
```python
from typing import List

def process_frames(frames: List[List[dict]]) -> None:
    # Initialize an instance of ObjectTracker for each object in the first frame
    trackers = [ObjectTracker(obj['bbox'], obj['class_label']) for obj in frames[0]]
    
    # Loop over all frames after the first frame
    for i in range(1, len(frames)):
        current_frame = frames[i]
        
        # Update each tracker with the detections in the current frame
        for tracker in trackers:
            detection = None
            
            # Find the detection in the current frame that matches the current tracker
            for obj in current_frame:
                if tracker.matches(obj['bbox'], obj['class_label']):
                    detection = obj
                    break
            
            # Update the tracker with the detection, if found
            if detection:
                tracker.update(detection['bbox'])
            # If no matching detection is found, mark the tracker as lost
            else:
                tracker.mark_lost()
        
        # Add new trackers for any detections in the current frame that do not match any existing trackers
        for obj in current_frame:
            matched = False
            
            for tracker in trackers:
                if tracker.matches(obj['bbox'], obj['class_label']):
                    matched = True
                    break
            
            if not matched:
                trackers.append(ObjectTracker(obj['bbox'], obj['class_label']))
        
        # Remove any lost trackers
        trackers = [tracker for tracker in trackers if not tracker.is_lost()]
```

This function takes in the example input data as a list of lists, where each inner list represents the detected objects in a single frame of the video sequence. It initializes an instance of the ObjectTracker class for each object in the first frame, and then loops over all frames after the first frame. For each frame, it updates each tracker with the corresponding detection (if found) using the `update` method of the ObjectTracker class. It also adds new trackers for any detections in the current frame that do not match any existing trackers using the `append` method of the ObjectTracker class. Finally, it removes any lost trackers using a list comprehension.

Note that this is just one example implementation of how to use the ObjectTracker class in a video tracking pipeline. Depending on your specific use case, you may need to modify this function or the ObjectTracker class to better suit your needs.

#### Me:
Can you provide code for the called" matches" and "mark_lost" methods of your ObjectTracker?

#### ChatGPT
Sure! Here's an example implementation of the `matches` and `mark_lost` methods for the `ObjectTracker` class:
```python
class ObjectTracker:
    def __init__(self, bbox, class_label):
        self.bbox = bbox
        self.class_label = class_label
        self.id = uuid.uuid4().hex
        self.age = 0
        self.hits = 1
        self.consecutive_misses = 0
        self.lost = False

    def matches(self, bbox, class_label):
        # Check if the given bbox and class_label match the current tracker's bbox and class_label
        if self.class_label == class_label and bbox_iou(self.bbox, bbox) > 0.5:
            return True
        else:
            return False

    def mark_lost(self):
        self.lost = True

    # Other methods omitted for brevity
```

The `matches` method takes in a `bbox` and `class_label` of a detection from the current frame, and returns `True` if the detection matches the current tracker's `bbox` and `class_label`, or `False` otherwise. In this example implementation, we use the Intersection over Union (IoU) metric with a threshold of 0.5 to determine whether the bboxes overlap significantly enough to be considered a match. You can adjust the threshold value based on your specific needs.

The `mark_lost` method simply sets the `lost` attribute of the tracker to `True`, indicating that it has been lost and should be removed from the list of active trackers. Note that this is just one example implementation of how to define the `matches` and `mark_lost` methods for the `ObjectTracker` class. Depending on your specific requirements, you may need to modify or extend these methods to better suit your needs.

#### Me:
What is the code of the bbox_iou function

#### ChatGPT
Sure! Here's an example implementation of the `bbox_iou` function that computes the Intersection over Union (IoU) metric between two bounding boxes:

```python
def bbox_iou(bbox1, bbox2):
    # Calculate the coordinates of the intersection rectangle
    x1 = max(bbox1[0], bbox2[0])
    y1 = max(bbox1[1], bbox2[1])
    x2 = min(bbox1[2], bbox2[2])
    y2 = min(bbox1[3], bbox2[3])
    
    # If the intersection is empty, return 0
    if x2 < x1 or y2 < y1:
        return 0.0
    
    # Calculate the area of intersection rectangle
    intersection_area = (x2 - x1 + 1) * (y2 - y1 + 1)
    
    # Calculate the area of both bounding boxes
    bbox1_area = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
    bbox2_area = (bbox2[2] - bbox2[0] + 1) * (bbox2[3] - bbox2[1] + 1)
    
    # Calculate the union area
    union_area = bbox1_area + bbox2_area - intersection_area
    
    # Calculate the IoU
    iou = intersection_area / union_area
    
    return iou
```

The `bbox_iou` function takes in two bounding boxes `bbox1` and `bbox2` as lists of four elements `[x_min, y_min, x_max, y_max]`, where `x_min` and `y_min` represent the coordinates of the top-left corner, and `x_max` and `y_max` represent the coordinates of the bottom-right corner. The function first calculates the coordinates of the intersection rectangle between the two bounding boxes, and then calculates the area of the intersection rectangle, the area of both bounding boxes, and the union area. Finally, the function computes the IoU metric as the ratio of the intersection area to the union area, and returns the IoU value as a float between 0 and 1.

Note that this is just one example implementation of the `bbox_iou` function, and there are many other ways to calculate the IoU metric, depending on your specific requirements and use case.

#### Me:
can you provide code again but just  for the called "matches" and "mark_lost" and  also the "is_lost" methods, using the original init:
```
def __init__(self, object_id, bbox, class_label):
    """
    Initialize the ObjectTracker class.

    Args:
        object_id (int): Unique ID for the object.
        bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                        coordinates in image coordinates.
        class_label (str): The class label of the object.
    """
    self.object_id = object_id
    self.class_label = class_label
    self.kf = KalmanFilter(dim_x=4, dim_z=2)
    self.kf.x = np.array([bbox[0], bbox[1], 0, 0]).T
    self.kf.P *= 10.
    self.kf.R = np.diag([5., 5.])
    self.kf.F = np.array([[1., 0., 1., 0.],
                            [0., 1., 0., 1.],
                            [0., 0., 1., 0.],
                            [0., 0., 0., 1.]])
    self.kf.H = np.array([[1., 0., 0., 0.],
                            [0., 1., 0., 0.]])
    self.kf.Q = np.diag([0.1, 0.1, 0.01, 0.01])
    self.age = 1
    self.time_since_update = 0
    self.history = []
    self.hits = 1
    self.hit_streak = 1
    self.miss_streak = 0
    self.is_confirmed = False
    self.is_deleted = False
```

#### ChatGPT :
Sure, here are example implementations of the `matches`, `mark_lost`, and `is_lost` methods of the `ObjectTracker` class:
```python
class ObjectTracker:
    def __init__(self, object_id, bbox, class_label):
        """
        Initialize the ObjectTracker class.

        Args:
            object_id (int): Unique ID for the object.
            bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                            coordinates in image coordinates.
            class_label (str): The class label of the object.
        """
        self.object_id = object_id
        self.class_label = class_label
        self.kf = KalmanFilter(dim_x=4, dim_z=2)
        self.kf.x = np.array([bbox[0], bbox[1], 0, 0]).T
        self.kf.P *= 10.
        self.kf.R = np.diag([5., 5.])
        self.kf.F = np.array([[1., 0., 1., 0.],
                                [0., 1., 0., 1.],
                                [0., 0., 1., 0.],
                                [0., 0., 0., 1.]])
        self.kf.H = np.array([[1., 0., 0., 0.],
                                [0., 1., 0., 0.]])
        self.kf.Q = np.diag([0.1, 0.1, 0.01, 0.01])
        self.age = 1
        self.time_since_update = 0
        self.history = []
        self.hits = 1
        self.hit_streak = 1
        self.miss_streak = 0
        self.is_confirmed = False
        self.is_deleted = False

    def matches(self, bbox):
        """
        Determine if a new bounding box matches this object.

        Args:
            bbox (list): List of four float values [x1, y1, x2, y2] representing the bounding box
                            coordinates in image coordinates.

        Returns:
            bool: True if the bounding box matches this object, False otherwise.
        """
        if self.is_confirmed:
            # Calculate IoU between current object and new bounding box
            iou = bbox_iou(self.bbox, bbox)

            # Check if IoU is above a threshold
            if iou > 0.5:
                return True
        else:
            # Calculate Euclidean distance between current object and new bounding box
            dist = np.linalg.norm(self.kf.x[:2] - np.array([bbox[0], bbox[1]]))

            # Check if distance is below a threshold
            if dist < 50:
                return True

        return False

    def mark_lost(self):
        """
        Mark the object as lost.
        """
        self.is_confirmed = False
        self.time_since_update = 0
        self.miss_streak += 1

        if self.miss_streak > 3:
            self.is_deleted = True

    def is_lost(self):
        """
        Check if the object is lost.

        Returns:
            bool: True if the object is lost, False otherwise.
        """
        return self.time_since_update > 3 * self.hit_streak

```

The `matches` method takes in a new bounding box `bbox` as a list of four elements `[x1, y1, x2, y2]`, where `x1` and `y1` represent the coordinates of the top-left corner, and `

#### Me:
Why is self.kf.x = np.array([bbox[0], bbox[1], 0, 0]).T andn ot initialized also with bbox[2] and bbox[3] ?

#### ChatGPT:
The reason why `self.kf.x` is initialized with only `bbox[0]` and `bbox[1]` and not with `bbox[2]` and `bbox[3]` is because the Kalman filter used for object tracking in this case is a 2-dimensional filter that works on the x and y coordinates of the bounding box only.

The Kalman filter used in this implementation is a constant velocity model, meaning that the state vector `x` consists of four elements: `[x, y, v_x, v_y]`, where `x` and `y` are the position coordinates of the center of the bounding box, and `v_x` and `v_y` are the velocity components in the x and y directions, respectively. The filter estimates the new position and velocity of the object at each time step based on the previous position and velocity estimates, as well as the measured position of the object in the current frame.

So, in this case, the x and y coordinates of the bounding box (`bbox[0]` and `bbox[1]`) are used to initialize the position component of the state vector, while the velocity components (`v_x` and `v_y`) are initialized to zero, since the initial velocity of the object is unknown.

## Code

https://gitlab.com/FutureTroglodyte/prototype_an_object_tracker_with_chatgpt

